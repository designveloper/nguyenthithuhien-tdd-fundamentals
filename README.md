**** TDD fundamentals ****

## 1. Introduction to Test-Driven Development (TDD)
### What kind of testing is this?
- There are many kinds of testing:
  + `beta testing`: providing a finished or nearly finished application to a group of users and seeing what problem they come up with.
  + `performance testing`: using profiling tools to measure if we're getting acceptable response times.
  + `stress testing` : one kind of performance testing where we want to know well the code works under heavy load, so this app works fine with 1, 100, 1000 or 10000 users at the same time.
  + `integration testing`: integrating with external other systems.
- This course talk about `unit tests` and more than `automated unit tests`.
- We will write code that tests our application code.
- Automated unit tests allows us to easily validate that any change we make.
### What is TDD?
- Test-Driven Development
- Doing unit testing is not the same thing as doing test-driven development.
- Unit testing - not TDD
  + Step 1: write logic
  + Step 2: Write tests
- TDD:
  + Step 1: Write tests
  + Step 2: write logic
  + Write test => Fail => Write application logic to past this test (minimum code to just pass the test) =>
- The tests drive our development. It drive what I write
- So TDD is not just development that uses unit tests.
- TDD as a process and technique, is all about the priority and the position.
### Common questions and concerns

## 2. Getting started
### Using unit testing frameworks
- Use xUnit frameworks
- SUnit (Smalltalk)
- JUnit (Java)
- NUnit (Python)
- PyUnit (Python)
- CppUnit (C++)
- OCUnit (Objective-C)

### Understanding assertions
- To assert is to sth positively
- If an assertion fails, then your code is broken or your assumptions about what is going on, they are fundamentally flawed.
- Assertion for only developer, not for end users.

### Using assertions in programming
### Creating a testing
### Creating a test in Eclipse

## 3. Working with tests
### The process of TDD revisited
- Red (fails) => Green (pass) => Refactor (make it right)
- TDD in a nutshell:
  + Write a test
  + Watch the test fail
  + Write application logic - as simple as possible
  + Pass the test
  + Refactor, removing duplication
  + Pass and test again.

### Adding tests and removing duplication
### Testing privacy
### Creating multiple test methods
### Naming unit tests and test methods
- Use name of unit being tested, and add specifics
- Some language require 'test' before name of test methods
- Use camel casing or underscores

## 4. Individual Techniques:
### Testing return values
- should return a true result
- should return true if do method successfully
- should return false if do method failed.

### Creating a test for expected exceptions
- Creating methods that deal with incorrect input or some other issue by throwing a known exception

### Setting up and tearing down
- `@Before setUp()`: will run before test
- `@After tearDown()`: will run after test
- Test fixture
  + setUpBeforeClass();
  + setUp();
  + testOne();
  + tearDown();
  + setUp();
  + testOne();
  + tearDown();
  + tearDownAfterClass()

### Common questions on individual tests
- Don't need to test getters and setters if they are simple. Only if they could meaningfully break
- Generallly, don't test private methods. Some unit testing frameworks support it.
  + Typically, just test public methods that proves the private methods works.
- Combine multiple test classes:
  + Use Test Suite:

## 5. Additional Topics
### Introducing mock objects
- Typical reasons for Mock Objects:
  + Real object hasn't been written yet
  + What you're calling has a UI/needs human interaction
  + Slow or difficult to set up
  + External resource: file system, database, network, printer
- Fake Objects and Mock Objects
  + Fake Objects
    + Match the original (or intended) method implementations
    + Return pre-arranged results
  + Mock Object also verify interaction:
    + Assert the expected values form unit under test
- Mock Object Frameworks:
  + Provide structure for defining mock Objects
    + Sometimes removing need to create custom class
  + Can aid in generating (or autogenerating) method stubs
  + Often provide prearranged mock objects
    + For file stream, console, network, printer equivalents
  + jMock framework: `jmock.org`
### Measuring code coverage
### TDD recommendations
